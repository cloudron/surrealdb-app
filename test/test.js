#!/usr/bin/env node

/* jshint esversion: 8 */
/* jslint node:true */
/* global it:false */
/* global xit:false */
/* global describe:false */
/* global before:false */
/* global after:false */

'use strict';

require('chromedriver');

const execSync = require('child_process').execSync,
    expect = require('expect.js'),
    path = require('path'),
    { Builder } = require('selenium-webdriver'),
    { Options } = require('selenium-webdriver/chrome');

describe('Application life cycle test', function () {
    this.timeout(0);

    const LOCATION = 'test';
    const EXEC_ARGS = { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' };
    const VERSION_PREFIX = 'surrealdb-';
    const EXPECTED_VERSION = '1.5.1';
    const ROOT_USER = {
        username: 'root',
        password: 'root'
    };
    const NS = 'test';
    const DB = 'test';
    const TABLE_NAME = 't';

    let browser, app, token;

    before(function () {
        browser = new Builder().forBrowser('chrome').setChromeOptions(new Options().windowSize({ width: 1280, height: 1024 })).build();
    });

    after(function () {
        browser.quit();
    });

    function sleep(millis) {
        return new Promise(resolve => setTimeout(resolve, millis));
    }

    function getAppInfo() {
        const inspect = JSON.parse(execSync('cloudron inspect'));
        app = inspect.apps.filter(function (a) { return a.location === LOCATION || a.location === LOCATION + '2'; })[0];
        expect(app).to.be.an('object');
    }

    async function healthCheck() {
        const response = await fetch(`https://${app.fqdn}/health`);
        expect(response.ok).to.be.true;
    }

    async function getAppVersion() {
        const response = await fetch(`https://${app.fqdn}/version`);

        if (!response.ok) {
            throw new Error(`HTTP error! Status: ${response.status}`);
        }

        const data = await response.text();
        const expected = VERSION_PREFIX + EXPECTED_VERSION;

        expect(data).to.equal(expected);
    }

    async function signIn() {
        const response = await fetch(`https://${app.fqdn}/signin`, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
            },
            body: JSON.stringify({
                user: ROOT_USER.username,
                pass: ROOT_USER.password
            })
        });

        if (!response.ok) {
            throw new Error(`HTTP error! Status: ${response.status}`);
        }

        const data = await response.json();

        expect(data).to.be.an('object');
        expect(data.code).to.equal(200);
        expect(data).to.have.property('token');
        expect(data.token).to.be.a('string');

        token = data.token;
    }

    async function createRecord() {
        const response = await fetch(`https://${app.fqdn}/key/${TABLE_NAME}`, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Authorization': `Bearer ${token}`,
                NS,
                DB,
            },
            body: JSON.stringify({
                key: 'value'
            })
        });

        if (!response.ok) {
            throw new Error(`HTTP error! Status: ${response.status}`);
        }

        const data = await response.json();

        expect(data).to.be.an('object');

        const result = data[0];

        expect(result).to.be.an('object');
        expect(result.status).to.equal('OK');

        const record = result.result[0];
        expect(record).to.eql({
            id: record.id,
            key: 'value'
        });
    }

    async function queryRecords(expected_count) {
        const QUERY = `SELECT count() FROM ${TABLE_NAME}`;

        const response = await fetch(`https://${app.fqdn}/sql`, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Authorization': `Bearer ${token}`,
                NS,
                DB,
            },
            body: QUERY
        });

        if (!response.ok) {
            throw new Error(`HTTP error! Status: ${response.status}`);
        }

        const data = await response.json();

        expect(data).to.be.an('object');

        const result = data[0];

        expect(result).to.be.an('object');
        expect(result.status).to.equal('OK');

        const record = result.result[0];
        expect(record).to.eql({ count: expected_count });
    }

    xit('build app', function () { execSync('cloudron build', EXEC_ARGS); });
    it('install app', async function () { execSync(`cloudron install --location ${LOCATION}`, EXEC_ARGS); });

    it('can get app information', getAppInfo);
    it('healthCheck', healthCheck);
    it('can get app version', getAppVersion);
    
    it('can set env variables to set root user', async function () {
        execSync(`cloudron env set SURREAL_USER=${ROOT_USER.username} --app ${app.id}`, EXEC_ARGS);
        execSync(`cloudron env set SURREAL_PASS=${ROOT_USER.password} --app ${app.id}`, EXEC_ARGS);
        // wait when all services are up and running
        await sleep(20000);
    });
    it('healthCheck', healthCheck);
    it('can sign in', signIn);
    it('can create record', createRecord);
    it('can query records', queryRecords.bind(null, 1));

    it('can restart app', async function () {
        execSync(`cloudron restart --app ${app.id}`, EXEC_ARGS);
        await sleep(20000);
    });

    it('healthCheck', healthCheck);
    it('can sign in', signIn);
    it('can query records', queryRecords.bind(null, 1));

    it('backup app', function () { execSync(`cloudron backup create --app ${app.id}`, EXEC_ARGS); });
    it('restore app', async function () {
        const backups = JSON.parse(execSync(`cloudron backup list --raw --app ${app.id}`));
        execSync(`cloudron uninstall --app ${app.id}`, EXEC_ARGS);
        execSync(`cloudron install --location ${LOCATION}`, EXEC_ARGS);
        getAppInfo();
        execSync(`cloudron restore --backup ${backups[0].id} --app ${app.id}`, EXEC_ARGS);
        // wait when all services are up and running
        await sleep(20000);
    });

    it('can get app information', getAppInfo);
    it('healthCheck', healthCheck);
    it('can sign in', signIn);
    it('can query records', queryRecords.bind(null, 1));

    it('move to different location', async function () {
        execSync(`cloudron configure --location ${LOCATION}2 --app ${app.id}`, EXEC_ARGS);
        // wait when all services are up and running
        await sleep(20000);
    });
    it('can get app information', getAppInfo);
    it('healthCheck', healthCheck);
    it('can sign in', signIn);
    it('can query records', queryRecords.bind(null, 1));

    it('uninstall app', async function () {
        execSync(`cloudron uninstall --app ${app.id}`, EXEC_ARGS);
    });

    // test update
    xit('can install app for update', async function () {
        execSync(`cloudron install --appstore-id com.surrealdb --location ${LOCATION}`, EXEC_ARGS);
        // wait when all services are up and running
        await sleep(20000);
    });
    xit('can get app information', getAppInfo);
    xit('healthCheck', healthCheck);
    xit('can sign in', signIn);
    xit('can query records', queryRecords.bind(null, 1));

    xit('can update', function () { execSync(`cloudron update --app ${app.id}`, EXEC_ARGS); });

    xit('uninstall app', async function () {
        execSync(`cloudron uninstall --app ${app.id}`, EXEC_ARGS);
    });
});

