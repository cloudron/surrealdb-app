By default, the application starts with all capabilities (`--allow-auth`) but no root user.

In order to provide a root user, you can define the following env variables using Cloudron CLI (`cloudron env`):

* `SURREAL_USER=` to define root username (`root` is recommended)
* `SURREAL_PASS=` to define root password (a strong password is recommended)

Note: any env variable can be set, you can deny some capabilities, enable strict mode, etc...  
