# SurrealDB Cloudron App

This repository contains the Cloudron app package source for [SurrealDB](https://surrealdb.com/).

## Installation

[![Install](https://cloudron.io/img/button.svg)](https://cloudron.io/button.html?app=com.surrealdb)

or using the [Cloudron command line tooling](https://cloudron.io/references/cli.html)

```
cloudron install --appstore-id com.surrealdb
```

## Building

The app package can be built using the [Cloudron command line tooling](https://cloudron.io/references/cli.html).

```
cd surrealdb-app

cloudron build
cloudron install
```

## Testing

The e2e tests are located in the `test/` folder and require [nodejs](http://nodejs.org/). They are creating a fresh build, install the app on your Cloudron, perform tests, backup, restore and test if the repos are still ok. The tests expect port 29418 to be available.

```
cd surrealdb-app/test

npm install
USERNAME=<cloudron username> PASSWORD=<cloudron password> mocha --bail test.js
```

## Update checklist

* [ ] Upgrade `VERSION` variable in `Dockerfile`
* [ ] Upgrade `version` in `CloudronManifest.json` 
* [ ] Change `EXPECTED_VERSION` in `test/test.js`
* [ ] Add release notes in `CHANGELOG.md`
