FROM cloudron/base:4.2.0@sha256:46da2fffb36353ef714f97ae8e962bd2c212ca091108d768ba473078319a47f4

RUN mkdir -p /app/code /app/pkg
WORKDIR /app/code

ARG VERSION=1.5.1

RUN curl -sSf https://install.surrealdb.com | sh -s -- --version v${VERSION}

# configure supervisor
COPY supervisor/ /etc/supervisor/conf.d/
RUN sed -e 's,^logfile=.*$,logfile=/run/supervisord.log,' -i /etc/supervisor/supervisord.conf

COPY start.sh /app/pkg/

CMD [ "/app/pkg/start.sh" ]