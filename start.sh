#!/bin/bash

set -eu

echo "=> changing ownership"
chown -R cloudron:cloudron /app/data

echo "=> starting surrealdb"
exec /usr/bin/supervisord --configuration /etc/supervisor/supervisord.conf --nodaemon -i surrealdb